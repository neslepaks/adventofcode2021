(defun read-positions ()
  (with-open-file (fp "input")
    (read-from-string
     (concatenate 'string "(" (substitute #\Space #\, (read-line fp)) ")"))))

(defun day7-1 ()
  (let ((positions (read-positions)))
    (loop for pos from (apply #'min positions) to (apply #'max positions)
	  minimizing (reduce #'+ (mapcar (lambda (n) (abs (- n pos)))
					 positions)))))

(defun fuelcalc (n pos)
  (loop for i from (min n pos) to (max n pos) summing (- i (min n pos))))

(defun day7-2 ()
  (let ((positions (read-positions)))
    (loop for pos from (apply #'min positions) to (apply #'max positions)
	  minimizing (reduce #'+ (mapcar (lambda (n) (fuelcalc n pos))
					 positions)))))
