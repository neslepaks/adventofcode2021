(defun day1-1 ()
  (with-open-file (fp "input")
    (let (last (incr 0) (decr 0))
      (loop for line = (read-line fp nil nil) while line
	 do (let ((n (parse-integer line)))
	      (when last
		(if (> n last)
		    (incf incr)
		    (incf decr)))
	      (setf last n)))
      (values incr decr))))

(defun day1-2 ()
  (with-open-file (fp "input")
    (let (last n2 n3 (incr 0) (decr 0))
      (loop for line = (read-line fp nil nil) while line
	 do (let ((n (parse-integer line)))
	      (when (and n2 n3)
		(when last
		  (if (> (+ n n2 n3) last)
		      (incf incr)
		      (incf decr)))
		(setf last (+ n n2 n3)))
	      (setf n3 n2
		    n2 n)))
      (values incr decr))))
      
    
