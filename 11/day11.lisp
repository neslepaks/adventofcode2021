(defun read-octopodes ()
  (with-open-file (fp "input")
    (loop repeat 10
	  collect (loop repeat 11
			for char = (read-char fp)
			when (char/= char #\Newline)
			  collect (- (char-code char) (char-code #\0))))))

;; So my idea here is for each generation we first increase all numbers.  Then
;; we check if we have any 10s, and if so we increase its neighbours and turn
;; it into something else (f for flashed maybe).  Repeat this until there are
;; no >9s left.  Finally turn every f to 0.

(defun energize-neighbours (grid)
  (flet ((incf-at (x y)
	   (when (and (<= 0 x 9) (<= 0 y 9))
	     (unless (eql #\f (nth x (nth y grid)))
	       (incf (nth x (nth y grid)))))))
    (loop for line in grid
	  for y upfrom 0
	  do (loop for octopus in line
		   for x upfrom 0
		   do (when (and (not (eql #\f octopus)) (< 9 octopus))
			(setf (nth x (nth y grid)) #\f)
			(incf-at (1- x) (1- y))
			(incf-at x (1- y))
			(incf-at (1+ x) (1- y))
			(incf-at (1- x) y)
			(incf-at (1+ x) y)
			(incf-at (1- x) (1+ y))
			(incf-at x (1+ y))
			(incf-at (1+ x) (1+ y))
			;; It shouldn't be necessary to build a giant recursion
			;; call tree here; all state is on the board.  Return
			;; and do another pass.
			(return-from energize-neighbours
			  (energize-neighbours grid)))))))

(defun evolve (generation grid flash-count &optional find-all-flashes)
  (if (and (= generation 100) (not find-all-flashes))
      flash-count
      ;; First increase everything.
      (let ((new-grid (loop for line in grid
			    collect (loop for octopus in line
					  collect (1+ octopus)))))
	;; Then process the over-9s.
	(energize-neighbours new-grid)
	;; Check for synchronized flash for part 2.
	(when (and find-all-flashes
		   (loop for line in new-grid
			 always (every (lambda (o) (eql #\f o)) line)))
	  (return-from evolve (1+ generation)))
	;; Turn fs back to 0s and go around.
	(let* ((counter 0)
	       (final-grid (loop for line in new-grid
				 collect (loop for octopus in line
					       collect (if (eql octopus #\f)
							   (progn
							     (incf counter)
							     0)
							   octopus)))))
	  (evolve (1+ generation)
		  final-grid
		  (+ flash-count counter)
		  find-all-flashes)))))

(defun day11-1 ()
  (evolve 0 (read-octopodes) 0))

(defun day11-2 ()
  (evolve 0 (read-octopodes) 0 t))
