(defun parse-dot (line)
  (read-from-string
   (concatenate 'string "(" (substitute #\Space #\, line) ")")))

(defun parse-fold (line)
  (let ((pos (position #\= line)))
    (list (char line (1- pos))
	  (parse-integer line :start (1+ pos)))))

(defun read-dots ()
  (with-open-file (fp "input")
    (let ((dots (loop for line = (read-line fp)
		      while (string/= line "")
		      collect (parse-dot line)))
	  (folds (loop for line = (read-line fp nil nil)
		       while line
		       collect (parse-fold line))))
      (list dots folds))))

(defun fold (paper type where)
  (remove-duplicates
   (loop for (x y) in paper
	 when (and (char= type #\y) (<= y where))
	   collect (list x y)
	 when (and (char= type #\y) (> y where))
	   collect (list x (abs (- (* where 2) y)))
	 when (and (char= type #\x) (<= x where))
	   collect (list x y)
	 when (and (char= type #\x) (> x where))
	   collect (list (abs (- (* where 2) x)) y))
   :test #'equal)))

(defun day13-1 ()
  (destructuring-bind (paper folds) (read-dots)
    (destructuring-bind (fold-type fold-line) (first folds)
      (length (fold paper fold-type fold-line)))))

(defun print-paper (paper)
  (loop for y from 0 to (apply #'max (mapcar #'second paper))
	do (loop for x from 0 to (apply #'max (mapcar #'first paper))
		 do (if (find (list x y) paper :test #'equal)
			(format t "#")
			(format t "."))
		 finally (format t "~%"))))

(defun day13-2 ()
  (destructuring-bind (paper folds) (read-dots)
    (loop for (fold-type fold-line) in folds
	  for new-paper = (fold paper fold-type fold-line)
	    then (fold new-paper fold-type fold-line)
	  finally (print-paper new-paper))))
