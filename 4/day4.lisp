(defun read-board ()
  (with-open-file (fp "input")
    (let ((draws (substitute #\Space #\, (read-line fp nil nil)))
	  (boards (loop for line = (read-line fp nil nil)
		     while line
		     if (string= line "")
		     collect (loop repeat 5
				collect (loop repeat 5
					   collect (read fp))))))
      (with-input-from-string (drawfp draws)
	(values (loop for num = (read drawfp nil nil) while num collect num)
		boards)))))

(defun drawn (n draws)
  (loop for i from 0 to (1- n) collect (nth i draws)))

(defun unmarked (numbers board)
  (let (nums)
    (loop for row in board
       do (loop for num in row
	     do (unless (find num numbers)
		  (push num nums))))
    nums))

(defun is-bingo (numbers board)
  (loop for row in board
     do (let ((isect (intersection numbers row)))
	  (when (= 5 (length isect))
	    (return-from is-bingo (* (apply #'+ (unmarked numbers board))
				     (first (last numbers)))))))
  (loop for column from 0 to 4
     do (let ((isect (intersection numbers (mapcar (lambda (r) (nth column r))
						   board))))
	  (when (= 5 (length isect))
	    (return-from is-bingo (* (apply #'+ (unmarked numbers board))
				     (first (last numbers))))))))

(defun day4-1 ()
  (multiple-value-bind (draws boards) (read-board)
    (loop for i upfrom 5
       do (loop for board in boards
	     do (let ((bingo (is-bingo (drawn i draws) board)))
		  (when bingo
		    (return-from day4-1 bingo)))))))

(defun day4-2 ()
  (let (winners)
    (multiple-value-bind (draws boards) (read-board)
      (loop for i upfrom 5
	 do (loop for board in boards
	       do (let ((bingo (is-bingo (drawn i draws) board)))
		    (when bingo
		      (push board winners)
		      (unless (set-difference boards winners :test #'equal)
			(return-from day4-2 bingo)))))))))
