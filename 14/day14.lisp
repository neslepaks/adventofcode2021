(defun parse-rule (rule)
  (remove #\- (remove #\> (remove #\Space (map 'list #'identity rule)))))

(defun read-polymers ()
  (with-open-file (fp "input")
    (let ((template (map 'list #'identity (read-line fp))))
      (assert (string= "" (read-line fp)))
      (list template
	    (loop for rule = (read-line fp nil nil)
		  while rule
		  collect (parse-rule rule))))))

(defun apply-template (template rules)
  (let (new-template)
    (loop for idx upfrom 0
	  for c1 in template
	  do (let ((c2 (nth (1+ idx) template)))
	       (when c2
		 ;(format t "finding ~a~%" (list c1 c2))
		 (let ((rule (find (list c1 c2)
				   rules
				   :key (lambda (r) (subseq r 0 2))
				   :test #'equal)))
		   (when rule
		     (when (zerop idx)
		       (push (first rule) new-template))
		     (push (third rule) new-template)
		     (push (second rule) new-template))))))
    (nreverse new-template)))

(defun run-steps (number-of-steps)
  (destructuring-bind (template rules) (read-polymers)
    (let ((bigass-template
	    (loop repeat number-of-steps
		  for new-template = (apply-template template rules)
		    then (apply-template new-template rules)
		  finally (return new-template))))
      (let ((hash (make-hash-table)))
	(loop for element in bigass-template
	      do (incf (gethash element hash 0)))
	(destructuring-bind (min max) (loop for v being the hash-value of hash
					    minimizing v into h-min
					    maximizing v into h-max
					    finally (return (list h-min
								  h-max)))
	  (- max min))))))

(defun day14-1 ()
  (run-steps 10))

(defun hash-items (hash-table)
  (loop for k being the hash-keys of hash-table using (hash-value v)
	collect (list k v)))

;; Full disclosure: I had to google this one for some hints, as I couldn't
;; figure out how to iterate without storing...

(defun element-count (steps)
  (let ((pair-count (make-hash-table :test #'equal))
	(elt-count (make-hash-table))
	(rules (make-hash-table :test #'equal)))
    (destructuring-bind (template rule-list) (read-polymers)
      (dolist (rule rule-list)
	(setf (gethash (subseq rule 0 2) rules) (third rule)))
      (dolist (elt template)
	(incf (gethash elt elt-count 0)))
      (loop for idx upfrom 0
	    for c1 in template
	    do (let ((c2 (nth (1+ idx) template)))
		 (when c2
		   (incf (gethash (list c1 c2) pair-count 0)))))
      (loop repeat steps
	    do (loop for (pair count) in (hash-items pair-count)
		     do (when (> count 0)
			  (decf (gethash pair pair-count) count)
			  (let ((new (gethash pair rules)))
			    (incf (gethash new elt-count 0) count)
			    (incf (gethash (list (first pair) new)
					   pair-count 0)
				  count)
			    (incf (gethash (list new (second pair))
					   pair-count 0)
				  count)))))
      (loop for v being the hash-values of elt-count
	    minimizing v into v-min
	    maximizing v into v-max
	    finally (return (- v-max v-min))))))

(defun day14-2 ()
  (element-count 40))
