(defun read-chunks ()
  (with-open-file (fp "input")
    (loop for line = (read-line fp nil nil)
	  while line
	  collect line)))

(defparameter *starts* '(#\( #\[ #\< #\{))
(defparameter *ends* '(#\) #\] #\> #\}))

(define-condition chunk-parse-error (condition)
  ((char :initarg :char)))

(defun parse-line (line)
  (let (stack)
    (loop for char across line
	  do (cond ((find char *starts*)
		    (push (elt *ends* (position char *starts*)) stack))
		   ((find char *ends*)
		    (if (char= char (first stack))
			(pop stack)
			(error 'chunk-parse-error :char char)))
		   (t (error "junk"))))
    stack))
  
(defun day10-1 ()
  (let ((lines (read-chunks))
	errors)
    (loop for line in lines
	  do (handler-case (parse-line line)
	       (chunk-parse-error (condition)
		 (push (slot-value condition 'char) errors))))
    (loop for error in errors
	  summing (ecase error
		    (#\) 3)
		    (#\] 57)
		    (#\} 1197)
		    (#\> 25137)))))

(defun score-line (line-stack)
  (let ((score 0))
    (loop for end in line-stack
	  do (setf score (+ (* 5 score) (ecase end
					  (#\) 1)
					  (#\] 2)
					  (#\} 3)
					  (#\> 4)))))
    score))

(defun day10-2 ()
  (let (scores)
    (loop for line in (read-chunks)
	  do (let ((line-stack (handler-case (parse-line line)
				 (chunk-parse-error ()
				   nil))))
	       (when line-stack
		 (push (score-line line-stack) scores))))
    (nth (floor (length scores) 2) (sort scores #'<))))
