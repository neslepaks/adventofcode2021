(defun parse-line (line)
  ;; 2,1 -> 3,2
  (with-input-from-string (fp (substitute #\Space #\, line))
    (let ((vals (loop repeat 5 collect (read fp))))
      (remove '-> vals))))

(defun read-coordinates (&optional only-straight)
  (with-open-file (fp "input")
    (let (coordinates)
      (loop for line = (read-line fp nil nil)
	    while line
	    do (destructuring-bind (x1 y1 x2 y2) (parse-line line)
		 (if only-straight
		     (when (or (= x1 x2) (= y1 y2))
		       (push (list x1 y1 x2 y2) coordinates))
		     (push (list x1 y1 x2 y2) coordinates))))
      coordinates)))
	 
(defun day5-1 ()
  (let ((cache (make-hash-table :test #'equal)))
    (loop for (x1 y1 x2 y2) in (read-coordinates t)
	  do (loop for x from (min x1 x2) to (max x1 x2)
		   do (loop for y from (min y1 y2) to (max y1 y2)
			    do (incf (gethash (list x y) cache 0)))))
    (loop for v being the hash-value of cache
	  counting (>= v 2))))

(defun range (a b)
  (if (> a b)
      (loop for x downfrom a to b collect x)
      (loop for x from a to b collect x)))

(defun day5-2 ()
  (let ((cache (make-hash-table :test #'equal)))
    (loop for (x1 y1 x2 y2) in (read-coordinates)
	  do (cond ((= x1 x2)
		    (loop for y from (min y1 y2) to (max y1 y2)
			  do (incf (gethash (list x1 y) cache 0))))
		   ((= y1 y2)
		    (loop for x from (min x1 x2) to (max x1 x2)
			  do (incf (gethash (list x y1) cache 0))))
		   (t
		    (loop for x in (range x1 x2)
			  for y in (range y1 y2)
			  do (incf (gethash (list x y) cache 0))))))
    (loop for v being the hash-value of cache
	  counting (>= v 2))))
