(defun read-ages ()
  (with-open-file (fp "input")
    (let ((line (substitute #\Space #\, (read-line fp))))
      (with-input-from-string (lfp line)
	(loop for fish = (read lfp nil nil)
	      while fish
	      collect fish)))))

(defun process-fish-age (n)
  (if (= n 0)
      6
      (1- n)))

(defun day6-1 ()
  (let ((fishes (read-ages)))
    (loop repeat 80
	  do (let ((zeros (count 0 fishes)))
	       (setf fishes (append (mapcar #'process-fish-age fishes)
				    (loop repeat zeros collect 8)))))
    (length fishes)))

(defun day6-2 ()
  (let ((age-buckets (loop for i from 8 downto 0 collect (cons i 0))))
    (loop for fish in (read-ages)
	  do (incf (cdr (assoc fish age-buckets))))
    (loop repeat 256
	  do (let ((copy (loop for (a . b) in age-buckets collect (cons a b))))
	       (loop for i downfrom 8 to 0
		     do (let ((n (cdr (assoc i copy))))
			  (if (> i 0)
			      (progn
				(decf (cdr (assoc i age-buckets)) n)
				(incf (cdr (assoc (1- i) age-buckets)) n))
			      (progn
				(decf (cdr (assoc 0 age-buckets)) n)
				(incf (cdr (assoc 6 age-buckets)) n)
				(incf (cdr (assoc 8 age-buckets)) n)))))))
    (apply #'+ (mapcar #'cdr age-buckets))))
