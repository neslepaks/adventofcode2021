(defun parse-line (line)
  (loop for char across line
	collect (- (char-code char) (char-code #\0))))

(defun read-topology ()
  (with-open-file (fp "input")
    (loop for line = (read-line fp nil nil)
	  while line
	  collect (parse-line line))))

(defun find-low-points (topology)
  (let ((max-y (1- (length topology)))
	(max-x (1- (length (first topology))))
	low-points)
    (flet ((n-at (x y)
	     (if (or (< x 0) (< y 0) (> x max-x) (> y max-y))
		 10
		 (nth x (nth y topology)))))
      (loop for y upfrom 0
	    for line in topology
	    do (loop for x upfrom 0
		     for num in line
		     when (< num (min (n-at x (1- y)) (n-at (1+ x) y)
				      (n-at (1- x) y) (n-at x (1+ y))))
		       do (push (list num x y) low-points))))
    low-points))

(defun day9-1 ()
  (apply #'+ (mapcar #'1+ (mapcar #'first (find-low-points (read-topology))))))

(defvar *visited*)

(defun walk-basin (x y topology)
  (unless (find (list x y) *visited* :test #'equal)
    (push (list x y) *visited*)
    (let ((max-y (1- (length topology)))
	  (max-x (1- (length (first topology)))))
      (flet ((n-at (x y)
	       (if (or (< x 0) (< y 0) (> x max-x) (> y max-y))
		   10
		   (nth x (nth y topology)))))
	(let* ((around `((,x ,(1- y)) (,x ,(1+ y)) (,(1- x) ,y) (,(1+ x) ,y)))
	       (lookat (loop for xy in around
			     when (> 9 (apply #'n-at xy))
			       collect xy)))
	  (if lookat
	      (cons (list x y)
		    (loop for (new-x new-y) in lookat
			  nconc (walk-basin new-x new-y topology)))
	      (list (list x y))))))))

(defun day9-2 ()
  (let* ((topology (read-topology))
	 (low-points (find-low-points topology)))
    (let (*visited*)
      (let ((basins (loop for (num x y) in low-points
			  collect (walk-basin x y topology))))
	(apply #'* (subseq (sort (mapcar #'length basins) #'>) 0 3))))))
