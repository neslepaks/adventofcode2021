(defun read-caves ()
  (with-open-file (fp "input")
    (loop for line = (read-line fp nil nil)
	  while line
	  collect (let ((pos (position #\- line)))
		    (list (subseq line 0 pos) (subseq line (1+ pos)))))))

(defun find-neighbours (caves cave)
  (append
   (mapcar #'second
	   (remove-if-not (lambda (c) (string= (first c) cave)) caves))
   (mapcar #'first
	   (remove-if-not (lambda (c) (string= (second c) cave)) caves))))

(defun is-allowed (cave visited)
  (let ((cave-count (count cave visited :test #'string=))
	(smalls (remove-if-not (lambda (c) (lower-case-p (char c 0))) visited)))
    (and (string/= cave "start")
	 (>= 1 cave-count)
	 (or (= 0 cave-count)
	     (every (lambda (c)
		      (= 1 (count c visited :test #'string=)))
		    (remove cave smalls :test #'string=))))))

(defun walk-cave (caves start-cave visited &optional small-allowed-twice)
  (if (string= start-cave "end")
      (list (reverse (cons "end" visited)))
      (let* ((neighbours (find-neighbours caves start-cave))
	     (ok (remove-if (lambda (cave)
			      (and (lower-case-p (char cave 0))
				   (if small-allowed-twice
				       (not (is-allowed cave (cons start-cave
								   visited)))
				       (find cave visited :test #'string=))))
			    neighbours)))
	(loop for cave in ok
	      nconc (walk-cave caves
			       cave
			       (cons start-cave visited)
			       small-allowed-twice)))))

(defun day12-1 ()
  (length (walk-cave (read-caves) "start" nil)))

(defun day12-2 ()
  (length (walk-cave (read-caves) "start" nil t)))
