(defun split (elt seq)
  (let ((pos (position elt seq)))
    (if pos
	(cons (subseq seq 0 pos)
	      (split elt (subseq seq (1+ pos))))
	(list seq))))

(defun parse-line (line)
  (let ((fields (split #\Space line)))
    (list (subseq fields 0 10) (subseq fields 11))))

(defun read-digits ()
  (with-open-file (fp "input")
    (loop for line = (read-line fp nil nil)
	  while line
	  collect (parse-line line))))

(defun day8-1 ()
  (loop for (pattern output) in (read-digits)
	summing (count-if (lambda (x) (find x '(2 3 4 7)))
			  (mapcar #'length output))))

;;   0:      1:      2:      3:      4:
;;  aaaa    ....    aaaa    aaaa    ....
;; b    c  .    c  .    c  .    c  b    c
;; b    c  .    c  .    c  .    c  b    c
;;  ....    ....    dddd    dddd    dddd
;; e    f  .    f  e    .  .    f  .    f
;; e    f  .    f  e    .  .    f  .    f
;;  gggg    ....    gggg    gggg    ....
;;
;;   5:      6:      7:      8:      9:
;;  aaaa    aaaa    aaaa    aaaa    aaaa
;; b    .  b    .  .    c  b    c  b    c
;; b    .  b    .  .    c  b    c  b    c
;;  dddd    dddd    ....    dddd    dddd
;; .    f  e    f  .    f  e    f  .    f
;; .    f  e    f  .    f  e    f  .    f
;;  gggg    gggg    ....    gggg    gggg

;; Lengths:
;; 2: 1
;; 3: 7
;; 4: 4
;; 5: 2, 3, 5
;; 6: 0, 6, 9
;; 7: 8

;; - We know 1, 4, 7, 8 by length.
;; - We find 2 by having a unique missing segment.
;; - We find 3 becuse it's the only 5-segment that contains 7.
;; - 5 by being the last 5-segment.
;; - 9 by being the only 6-segment that contains 3.
;; - 6 being the remaining 6-segment that contains 5.
;; - 0 is the last.

(defun compile-pattern (pattern)
  (flet ((by-length (n)
	   (remove-if-not (lambda (p) (= n (length p))) pattern))
	 (by-onlymissing (eight)
	   (loop for letter across eight
		 when (= 9 (count-if (lambda (n) (find letter n)) pattern))
		   do (return (find-if-not (lambda (n) (find letter n))
					   pattern))))
	 (by-overlap (pattern pat)
	   (loop for lm in pattern
		 when (loop for elt across pat always (find elt lm))
		   do (return lm))))
    (let* ((one (first (by-length 2)))
	   (four (first (by-length 4)))
	   (seven (first (by-length 3)))
	   (eight (first (by-length 7)))
	   (two (by-onlymissing eight))
	   (three (by-overlap (by-length 5) seven))
	   (five (first (remove three (remove two (by-length 5)))))
	   (nine (by-overlap (by-length 6) three))
	   (six (by-overlap (remove nine (by-length 6)) five))
	   (zero (first (remove nine (remove six (by-length 6))))))
      (list zero one two three four five six seven eight nine))))

(defun find-output-number (pattern output)
  (let* ((numbers (compile-pattern pattern))
	 (digit-list (loop for pat in output
			   collect (position (sort pat #'char<)
					     numbers
					     :key (lambda (x) (sort x #'char<))
					     :test #'string=))))
    (parse-integer (format nil "~{~a~}" digit-list)))))

(defun day8-2 ()
  (loop for (pattern output) in (read-digits)
	summing (find-output-number pattern output)))
